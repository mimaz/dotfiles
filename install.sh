#!/usr/bin/env bash

DOTFILES="$PWD"

pushd $HOME > /dev/null

opt_root=0
opt_deinstall=0

while getopts "rd" arg
do
	case $arg in
		r) opt_root=1;;
		d) opt_deinstall=1;;
	esac
done

function __append_line() {
	LINE="$1"
	FILE="$HOME/$2"

	if [ $opt_deinstall -eq 0 ]
	then
		if [ ! -f "$FILE" ] || ! grep -q "$LINE" "$FILE"
		then
			mkdir -pv $(dirname "$FILE")
			echo adding \"$LINE\" to $FILE
			echo "$LINE" >> $FILE
		fi
	else
		if [ -f "$FILE" ] && grep -q "$LINE" "$FILE"
		then
			echo removing \"$LINE\" from $FILE
			sed -i "/$(echo $LINE | sed 's/\//\\\//g')/d" $FILE
		fi
	fi
}

function append_line() {
	__append_line "$1" "$2"

	if [ $opt_root -ne 0 ]
	then
		sudo cat << EOF | sudo bash -
opt_root=$opt_root
opt_deinstall=$opt_deinstall
$(declare -f __append_line)
__append_line "$(echo "$1" | sed 's/\$/\\\$/g')" "$2"
EOF
	fi
}

function link_root() {
	LINK="$(eval echo ~root)/$1"
	FILE="$HOME/$1"

	if [ $opt_root -eq 0 ]
	then
		return
	fi

	if [ $opt_deinstall -eq 0 ]
	then
		if ! sudo test -L "$LINK"
		then
			sudo mkdir -pv "$(dirname $LINK)"
			sudo ln -sv "$FILE" "$LINK"
		fi
	elif sudo test -L "$LINK"
	then
		sudo rm -vf $LINK
	fi
}

function git_clone() {
	URL="$1"
	DIR="$HOME/$2"

	if [ $opt_deinstall -eq 0 ]
	then
		if [ ! -d "$DIR" ]
		then
			mkdir -pv $(dirname $DIR)
			git clone "$URL" "$DIR"
		fi

		link_root "$2"
	elif [ -d "$DIR" ]
	then
		echo delete git repo $DIR
		rm -rf $DIR
	fi
}

append_line "source $DOTFILES/bashrc" ".bashrc"
append_line "\$include $DOTFILES/inputrc" ".inputrc"

if [ -f "$(which fzf)" ]
then
	fzf_share=$(dirname $(which fzf))/../share
	fzf_bash=$(find $fzf_share -regex ".*/fzf/.*key-bindings.bash" 2>/dev/null)

	if [ -f "$fzf_bash" ]
	then
		append_line "source $fzf_bash" ".bashrc"
	else
		echo fzf key-bindings.bash not found
	fi
else
	echo fzf was not installed
fi

git_clone "https://github.com/VundleVim/Vundle.vim.git" ".vim/bundle/Vundle.vim"

append_line "so $DOTFILES/vimrc" ".vim/vimrc"
append_line "so $DOTFILES/vimrc" ".config/nvim/init.vim"
vim +PluginInstall +qall

ls -1 .vim/bundle | while read plugin
	do
		link_root ".vim/bundle/$plugin"
	done

if [ $opt_deinstall -ne 0 ]
then
	ls -1 .vim/bundle | while read plugin
		do
			echo remove $plugin
			rm -rf .vim/bundle/$plugin
		done
fi

append_line "source-file $DOTFILES/tmux.conf" ".tmux.conf"
append_line "source $DOTFILES/gdbinit" ".gdbinit"

popd > /dev/null
