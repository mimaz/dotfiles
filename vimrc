set nocompatible
filetype off

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'VundleVim/Vundle.vim'

Plugin 'tpope/vim-fugitive'
Plugin 'preservim/nerdtree'
Plugin 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plugin 'junegunn/fzf.vim'
Plugin 'Vimjas/vim-python-pep8-indent'
Plugin 'rafcamlet/shadowmoth'
Plugin 'justinmk/vim-syntax-extra'

call vundle#end()
filetype plugin indent on

try
    colorscheme shadowmoth
catch
endtry

let mapleader = ","

if !isdirectory($HOME."/.vim/undo")
	call mkdir($HOME."/.vim/undo", "", 0700)
endif

set noswapfile
set undodir=~/.vim/undo
set undofile
set nohlsearch
set number rnu
set noexpandtab
set tabstop=8
set shiftwidth=4
set softtabstop=4

set cindent
set cinoptions=>s,Ls,:0,g0,t0,(0,u0,U1,w1,Ws,m1,j1,J1,P1

filetype plugin indent on

nnoremap <leader>q :q<CR>
nnoremap <leader>t :tabe<CR>
nnoremap <C-o> gT<CR>
nnoremap <C-p> gt<CR>

nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

nnoremap <S-h> :vertical resize -5<CR>
nnoremap <S-j> :resize -5<CR>
nnoremap <S-k> :resize +5<CR>
nnoremap <S-l> :vertical resize +5<CR>

nnoremap <expr><leader>s (&hls && v:hlsearch ? ':nohls' : ':set hls')."\n"
nnoremap <leader>f :NERDTreeToggle<CR>
nnoremap <leader>1 :set nonumber nornu<CR>
nnoremap <leader>2 :set number nornu<CR>
nnoremap <leader>3 :set number rnu<CR>

command! Susave w !sudo dd of=%
