export HISTCONTROL=ignoreboth:erasedups
shopt -s autocd
if command -v nvim &> /dev/null
then
	export EDITOR=nvim
	alias vim=nvim
	alias vi=nvim
elif command -v vim &> /dev/null
then
	export EDITOR=vim
	alias vi=vim
else
	export EDITOR=vi
fi
alias tmux="tmux -2"
bind '"\eu":"cd ..\n"'
bind '"\ee":"vim\n"'
bind '"\el":"ls -l\n"'
bind '"\eL":"ls -la\n"'
if [ "$EUID" -ne 0 ]
then
    PS1="\w\\$ "
    PROMPT_DIRTRIM=2
fi
